//
//  CustomImageViewWithCache.swift
//  InfyAssignment
//
//  Created by Mujib on 23/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

// This call is a subclass of UIImageView to add additioinal cache functionality.

import UIKit

// MARK: Global Constants

let imageCache = NSCache<NSString, UIImage>()

class CustomImageViewWithCache: UIImageView {
    
    // MARK: Variable
    
    var imageUrlString: String?
    
    // MARK: Methods
    
    func loadImageUsingUrlString(_ urlString: String, tableView: UITableView, indexpath: IndexPath) {
        let url = URL(string: urlString)
        imageUrlString = urlString
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                guard let data = data,
                    let imageToCache = UIImage(data: data) else {
                        return }
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                imageCache.setObject(imageToCache, forKey: urlString as NSString)
                tableView.reloadRows(at: [indexpath], with: .middle)
            }
        }.resume()
        
    }
}
