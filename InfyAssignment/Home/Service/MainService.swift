//
//  MainService.swift
//  InfyAssignment
//
//  Created by Mujib on 21/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

import Alamofire

class MainService {
    
    // MARK: Typealias
    
    typealias CompletionHandler = (MainData?, String?) -> Void
    
    // MARK: Constants
    
//    static let url: String = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    
/// Above is the original url provided but since there is some issue in hosting, I have hosted the same JSON on another temporary server with the URL as below
    
    static let urlString: String = "http://www.mocky.io/v2/5e9f2bc72d00004a00cb7a59"
    
    // MARK: Main Service call
    
    func fecthImageData(completionHandler: @escaping CompletionHandler) {
        
        let selfType = type(of: self)
        
/// As prefered, the initial attempt to fetch the data was the default API provided by Swift. But since the requirement is use as many frameworks as possible, i have added Alamofire instead.
        
/// Below code is kept put just to make sure both the attemots were made for this.
        
        //        if let url = URL(string: selfType.urlString) {
        //            let request = URLRequest(url: url)
        //            URLSession.shared.dataTask(with: request) { data, res, error in
        //
        //                if let response = try? JSONSerialization.jsonObject(with: data!, options: []) {
        //                    print(response)
        //                }
        //                if let data = data {
        //                    if let json = try? JSONDecoder().decode(MainData.self, from: data) {
        //                        print(json)
        //                    }
        //                }
        //            }.resume()
        //        }
        
        AF.request(selfType.urlString, method: .get).responseData { response in
            
            guard let decoded = response.data else {
                completionHandler(nil, NSLocalizedString("ServerResponseErrorKey", comment: "Server response decoding error"))
                return
            }
            do {
                let responseDecoded = try JSONDecoder().decode(MainData.self, from: decoded)
                completionHandler(responseDecoded,nil)
            } catch {
                completionHandler(nil, NSLocalizedString("ServerResponseErrorKey", comment: "Server response decoding error"))
            }
            
        }
    }
}
