//
//  HomeViewController.swift
//  InfyAssignment
//
//  Created by Mujib on 21/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
    
    // MARK: Constant
    
    static let cellIdentifier: String = "HomeCustomCell"
    static let mainThemeColor = #colorLiteral(red: 0.3023633823, green: 0.763811015, blue: 1, alpha: 1)
    static let mainThemeBGColor = #colorLiteral(red: 0.6216265634, green: 0.9216911757, blue: 1, alpha: 1)
    
    // MARK: Variable
    
    private var mainData: HomeViewModel?
    private var imageData = [ImageViewModel]()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(frame: self.view.bounds)
        loader.color = type(of: self).mainThemeColor
        loader.hidesWhenStopped = true
        return loader
    }()
    private lazy var loadingIndicatorView: UIView = {
        let view = UIView(frame: self.view.bounds)
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.alpha = 0.5
        return view
    }()
    
    // MARK: Lifecycle methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupUI()
        
    }
    
    // MARK: Methods
    
    private func setupUI() {
        let selftType = type(of: self)
        
        self.view.backgroundColor = selftType.mainThemeBGColor
        
        self.navigationItem.title = NSLocalizedString("TitleLoadingKey", comment: "loading the title")
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.barTintColor = selftType.mainThemeColor
        
        self.tableView.register(HomeCustomCell.self,
                                forCellReuseIdentifier: selftType.cellIdentifier);
        
        self.view.addSubview(self.loadingIndicatorView)
        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.startAnimating()
        
        MainService().fecthImageData { (response, error) in
            guard let mainData = response,
                let rows = mainData.rows else { return }
            self.mainData = HomeViewModel(data: mainData)
            for row in rows.compactMap({$0}) {
                if row.title != nil,
                    row.description != nil,
                    row.imageURL != nil {
                    self.imageData.append(ImageViewModel(data: row))
                }
            }
            if let title = self.mainData?.data?.title {
                self.navigationItem.title = title
            } else {
                self.navigationItem.title = NSLocalizedString("TitleLoadingErrorKey", comment: "title fetch error")
            }
            
            self.tableView.reloadData()
            self.loadingIndicator.stopAnimating()
            self.loadingIndicatorView.removeFromSuperview()
        }
    }
    
}

extension HomeViewController {
    
    // MARK: Table View Delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Do something when user interacts with the tableview
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension HomeViewController{
    
    // MARK: Table View DataSource methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imageData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: type(of: self).cellIdentifier) as? HomeCustomCell else { return UITableViewCell() }
        cell.setupUI(headingText: self.imageData[indexPath.row].data?.title,
                     subHeadingText: self.imageData[indexPath.row].data?.description,
                     imageText: self.imageData[indexPath.row].data?.imageURL,
                     tableView: tableView,
                     indexPath: indexPath)
        return cell
    }
    
}
