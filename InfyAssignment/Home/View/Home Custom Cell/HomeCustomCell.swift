//
//  HomeCustomCell.swift
//  InfyAssignment
//
//  Created by Mujib on 22/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

import UIKit

let cacheObject = NSCache<NSURL, UIImage>()

class HomeCustomCell: UITableViewCell {
    
    // MARK: Outlets
    
    private var mainImage = CustomImageViewWithCache()
    private var heading = UILabel()
    private var subHeading = UILabel()
    
    // MARK: Constants
    
    private let miscBackgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    private let miscFontColour = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    // MARK: Constructors
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Method
    
    func setupUI(headingText: String?, subHeadingText: String?, imageText: String?, tableView: UITableView, indexPath: IndexPath) {
        
        self.clipsToBounds = true
        
        if let urlString = imageText {
            self.addSubview(self.mainImage)
            self.mainImage.backgroundColor = self.miscBackgroundColor
            self.mainImage.translatesAutoresizingMaskIntoConstraints = false
            let constraints = [ NSLayoutConstraint(item: self.mainImage,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: 8),
                                NSLayoutConstraint(item: self.mainImage,
                                                   attribute: .trailing,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .trailing,
                                                   multiplier: 1,
                                                   constant: -8),
                                NSLayoutConstraint(item: self.mainImage,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .top,
                                                   multiplier: 1,
                                                   constant: 8)]
            
            NSLayoutConstraint.activate(constraints)
            self.mainImage.contentMode = .scaleAspectFit
            self.mainImage.loadImageUsingUrlString(urlString,
                                                   tableView: tableView,
                                                   indexpath: indexPath)
        }
        if let headertext = headingText {
            self.heading.backgroundColor = self.miscBackgroundColor
            self.heading.textColor = self.miscFontColour
            self.heading.font = .boldSystemFont(ofSize: 17)
            self.addSubview(self.heading)
            self.heading.translatesAutoresizingMaskIntoConstraints = false
            var topConstraint = NSLayoutConstraint()
            if self.subviews.contains(self.mainImage) {
                topConstraint = NSLayoutConstraint(item: self.heading,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: self.mainImage,
                                                   attribute: .bottom,
                                                   multiplier: 1,
                                                   constant: 0)
            } else {
                topConstraint = NSLayoutConstraint(item: self.heading,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .top,
                                                   multiplier: 1,
                                                   constant: 0)
            }
            let constraints = [ NSLayoutConstraint(item: self.heading,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: 8),
                                NSLayoutConstraint(item: self.heading,
                                                   attribute: .trailing,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .trailing,
                                                   multiplier: 1,
                                                   constant: -8),
                                topConstraint]
            
            NSLayoutConstraint.activate(constraints)
            
            self.heading.textColor = .white
            self.heading.text = headertext
        }
        if let subHeadingText = subHeadingText {
            self.addSubview(self.subHeading)
            self.subHeading.backgroundColor = self.miscBackgroundColor
            self.subHeading.textColor = self.miscFontColour
            self.subHeading.font = .systemFont(ofSize: 13)
            self.subHeading.translatesAutoresizingMaskIntoConstraints = false
            self.subHeading.numberOfLines = 0
            var topConstraint = NSLayoutConstraint()
            if self.subviews.contains(self.heading) {
                topConstraint = NSLayoutConstraint(item: self.subHeading,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: self.heading,
                                                   attribute: .bottom,
                                                   multiplier: 1,
                                                   constant: 0)
            } else {
                topConstraint = NSLayoutConstraint(item: self.subHeading,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .top,
                                                   multiplier: 1,
                                                   constant: 0)
            }
            
            let constraints = [ NSLayoutConstraint(item: self.subHeading,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: 8),
                                NSLayoutConstraint(item: self.subHeading,
                                                   attribute: .trailing,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .trailing,
                                                   multiplier: 1,
                                                   constant: -8),
                                topConstraint,
                                NSLayoutConstraint(item: self.subHeading,
                                                   attribute: .bottom,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .bottom,
                                                   multiplier: 1,
                                                   constant: -8)]
            
            NSLayoutConstraint.activate(constraints)
            self.subHeading.text = subHeadingText
            
        }
        
    }
}
