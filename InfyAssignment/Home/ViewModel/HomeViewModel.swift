//
//  HomeViewModel.swift
//  InfyAssignment
//
//  Created by Mujib on 21/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

struct HomeViewModel {
    
    let data: MainData?
    
    init(data: MainData) {
        self.data = data
    }
}

struct ImageViewModel {
    let data: ImageData?
    
    init(data: ImageData) {
        self.data = data
    }
}
