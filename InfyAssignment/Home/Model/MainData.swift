//
//  MainData.swift
//  InfyAssignment
//
//  Created by Mujib on 21/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

struct MainData: Codable {
    
    let title: String?
    let rows: [ImageData]?
}
