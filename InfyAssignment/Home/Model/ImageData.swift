//
//  ImageData.swift
//  InfyAssignment
//
//  Created by Mujib on 21/04/20.
//  Copyright © 2020 Mujib. All rights reserved.
//

struct ImageData: Codable {
    let title: String?
    let imageURL: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case imageURL = "imageHref"
        case description
    }
}
